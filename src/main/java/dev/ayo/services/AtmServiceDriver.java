package dev.ayo.services;

import java.util.Scanner;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import dev.ayo.bank.Account;
import dev.ayo.bank.User;
import dev.ayo.daos.AccountDaoImpl;
import dev.ayo.util.ConnectionUtil;


public class AtmServiceDriver {
	
	public static void main(String[] args) throws Exception {
	
		Account account = new Account();
		User user = new User();
		Scanner scanner = new Scanner(System.in);
		AccountDaoImpl acDao = new AccountDaoImpl();
		
	System.out.println("*********************");
	System.out.println("-Welcome to Ayo Bank-");
	System.out.println("*********************");

	
	System.out.println("Please enter your username to view your accounts");
	String uName = scanner.next();
	acDao.getAccountInformation(uName);
	
	System.out.println("\n");
	
	System.out.println("Please enter your username, account number, and deposit amount to deposit money");
	String uName1 = scanner.next();
	int actNum = scanner.nextInt();
	double deposit = scanner.nextDouble();
	acDao.deposit(uName1, actNum, deposit);
	
	System.out.println("\n");

	System.out.println("Please enter your username and account number, and withdrawl amount to withdraw money");
	String uName2 = scanner.next();
	int actNum2 = scanner.nextInt();
	double withdrawl = scanner.nextDouble();
	acDao.withdrawl(uName2, actNum2, withdrawl);
	
	System.out.println("\n");
	
	System.out.println("Please enter your the appropriate username and account number to Approve the Pending Acct.");
	String uName3 = scanner.next();
	int actNum3 = scanner.nextInt();
	acDao.approveAccount(uName3, actNum3);
	
	System.out.println("\n");
	
	System.out.println("Please enter your the appropriate username and account number to Reject the Pending Acct.");
	String uName4 = scanner.next();
	int actNum4 = scanner.nextInt();
	acDao.rejectAccount(uName4, actNum4);
	
	}
	
}
	
	
	
	
	
	
	
	
	


