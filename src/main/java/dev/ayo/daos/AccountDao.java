package dev.ayo.daos;

import dev.ayo.bank.Account;
import dev.ayo.bank.AccountStatus;
import dev.ayo.bank.User;

public interface AccountDao {
	
	public Account getAccountInformation(String username);
	public Account deposit(String username, int accountNumber, double deposit);
	public Account withdrawl(String username, int accountNumber, double withdrawl);
	public User createUser(String username, String password);
	public Account createAccount(String username, double initialBalance);
	public Account rejectAccount(String username, int accountNumber);
	public Account approveAccount(String username, int accountNumber);


}