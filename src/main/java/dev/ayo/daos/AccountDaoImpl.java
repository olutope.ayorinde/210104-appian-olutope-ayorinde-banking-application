package dev.ayo.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dev.ayo.bank.Account;
import dev.ayo.bank.AccountStatus;
import dev.ayo.bank.User;
import dev.ayo.util.ConnectionUtil;

public class AccountDaoImpl implements AccountDao {

	@Override
	public Account getAccountInformation(String username) {
		try {
			String sql = "SELECT * FROM BANK_ACCOUNTS WHERE USERNAME = ? ";
			Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pStatement = connection.prepareStatement(sql);
			pStatement.setString(1, username);
			ResultSet resultSet = pStatement.executeQuery();
			Account account = new Account(); 
			while(resultSet.next()) {
				account.setAccountNumber(resultSet.getInt("ACCOUNT_NUMBER"));
				account.setUsername(resultSet.getString("USERNAME"));
				account.setAccountBalance(resultSet.getDouble("ACCOUNT_BALANCE"));
				account.setStatus1(resultSet.getString("ACCOUNT_STATUS"));
				System.out.println(account);
			}
				
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public Account rejectAccount(String username, int accountNumber) {
		try {
			String sql = "SELECT * FROM BANK_ACCOUNTS WHERE USERNAME = ? AND ACCOUNT_NUMBER = ? AND ACCOUNT_STATUS = 'PENDING'";
			Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pStatement = connection.prepareStatement(sql);
			pStatement.setString(1, username);
			pStatement.setInt(2, accountNumber);
			ResultSet resultSet = pStatement.executeQuery();
			Account account = new Account(); 
			while(resultSet.next()) {
				
				account.setAccountNumber(resultSet.getInt("ACCOUNT_NUMBER"));
				account.setUsername(resultSet.getString("USERNAME"));
				account.setAccountBalance(resultSet.getDouble("ACCOUNT_BALANCE"));
				account.setStatus1(resultSet.getString("ACCOUNT_STATUS"));
				System.out.println(account);
			}
			String accountStatus = account.getStatus1();
			System.out.println("Your pre-transaction status: " + accountStatus);
			
			account.setStatus1("REJECTED");
			
			String newStatus = account.getStatus1();
			
			System.out.println("Account_Status has been Changed!");
			
			String sql2  = "UPDATE BANK_ACCOUNTS SET ACCOUNT_STATUS = ? WHERE USERNAME = ? AND ACCOUNT_NUMBER = ?";
			Connection connection2 = ConnectionUtil.getConnection();
			PreparedStatement pStatement2 = connection2.prepareStatement(sql2);
			pStatement2.setString(1, newStatus);
			pStatement2.setString(2, username);
			pStatement2.setInt(3, accountNumber);
			ResultSet resultSet2 = pStatement2.executeQuery();
			
			System.out.println("Current Account Status is: " + newStatus);
				
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return null;
}


	@Override
	public Account approveAccount(String username, int accountNumber) {
		try {
			String sql = "SELECT * FROM BANK_ACCOUNTS WHERE USERNAME = ? AND ACCOUNT_NUMBER = ? AND ACCOUNT_STATUS = 'PENDING'";
			Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pStatement = connection.prepareStatement(sql);
			pStatement.setString(1, username);
			pStatement.setInt(2, accountNumber);
			ResultSet resultSet = pStatement.executeQuery();
			Account account = new Account(); 
			while(resultSet.next()) {
				
				account.setAccountNumber(resultSet.getInt("ACCOUNT_NUMBER"));
				account.setUsername(resultSet.getString("USERNAME"));
				account.setAccountBalance(resultSet.getDouble("ACCOUNT_BALANCE"));
				account.setStatus1(resultSet.getString("ACCOUNT_STATUS"));
				System.out.println(account);
			}
			String accountStatus = account.getStatus1();
			System.out.println("Your pre-transaction status: " + accountStatus);
			
			account.setStatus1("APPROVED");
			
			String newStatus = account.getStatus1();
			
			System.out.println("Account_Status has been Changed!");
			
			String sql2  = "UPDATE BANK_ACCOUNTS SET ACCOUNT_STATUS = ? WHERE USERNAME = ? AND ACCOUNT_NUMBER = ?";
			Connection connection2 = ConnectionUtil.getConnection();
			PreparedStatement pStatement2 = connection2.prepareStatement(sql2);
			pStatement2.setString(1, newStatus);
			pStatement2.setString(2, username);
			pStatement2.setInt(3, accountNumber);
			ResultSet resultSet2 = pStatement2.executeQuery();
			
			System.out.println("Current Account Status is: " + newStatus);
				
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public Account createAccount(String username, double initialBalance) {
		try {		
			String sql = "INSERT INTO BANK_ACCOUNTS (USERNAME, ACCOUNT_BALANCE) VALUES (?, ?)";
			Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pStatement = connection.prepareStatement(sql);
			pStatement.setString(1, username);
			pStatement.setDouble(2, initialBalance);
			ResultSet resultSet = pStatement.executeQuery();
			Account account = new Account(); 
			while(resultSet.next()) {
				
				account.setAccountNumber(resultSet.getInt("ACCOUNT_NUMBER"));
				account.setUsername(resultSet.getString("USERNAME"));
				account.setAccountBalance(resultSet.getDouble("ACCOUNT_BALANCE"));
				account.setStatus1(resultSet.getString("ACCOUNT_STATUS"));
				System.out.println(account);
			}
//			System.out.println("Your new account details: User_Number= " + uNumber + ", Username= " + uName + " Password= " + pWord  );
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public Account deposit(String username, int accountNumber, double deposit) {
		try {
			String sql = "SELECT * FROM BANK_ACCOUNTS WHERE USERNAME = ? AND ACCOUNT_NUMBER = ?";
			Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pStatement = connection.prepareStatement(sql);
			pStatement.setString(1, username);
			pStatement.setInt(2, accountNumber);
			ResultSet resultSet = pStatement.executeQuery();
			Account account = new Account(); 
			while(resultSet.next()) {
				
				account.setAccountNumber(resultSet.getInt("ACCOUNT_NUMBER"));
				account.setUsername(resultSet.getString("USERNAME"));
				account.setAccountBalance(resultSet.getDouble("ACCOUNT_BALANCE"));
				account.setStatus1(resultSet.getString("ACCOUNT_STATUS"));
				System.out.println(account);
			}
			double balance = account.getAccountBalance();
			String accountStatus = account.getStatus1();
			System.out.println("Your pre-transaction balance: $" + balance);
			if(deposit <= 0) {
				System.out.println("Sorry! Invalid deposit amount.");
			} else if (accountStatus == "PENDING") {
				System.out.println("Sorry! Your account hasn't been approved yet.");
			} else if (accountStatus == "REJECTED") {
				System.out.println("Sorry! This is a rejected account.");
			} else {
				balance+= deposit;
				System.out.println("Deposit Successful!");
			}
			
			String sql2  = "UPDATE BANK_ACCOUNTS SET ACCOUNT_BALANCE = ? WHERE USERNAME = ? AND ACCOUNT_NUMBER = ?";
			Connection connection2 = ConnectionUtil.getConnection();
			PreparedStatement pStatement2 = connection2.prepareStatement(sql2);
			pStatement2.setDouble(1, balance);
			pStatement2.setString(2, username);
			pStatement2.setInt(3, accountNumber);
			ResultSet resultSet2 = pStatement2.executeQuery();
			
			System.out.println("Current Balance is: $" + balance);
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public Account withdrawl(String username, int accountNumber, double withdrawl) {
		try {
			String sql = "SELECT * FROM BANK_ACCOUNTS WHERE USERNAME = ? AND ACCOUNT_NUMBER = ?";
			Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pStatement = connection.prepareStatement(sql);
			pStatement.setString(1, username);
			pStatement.setInt(2, accountNumber);
			ResultSet resultSet = pStatement.executeQuery();
			Account account = new Account(); 
			while(resultSet.next()) {
				
				account.setAccountNumber(resultSet.getInt("ACCOUNT_NUMBER"));
				account.setUsername(resultSet.getString("USERNAME"));
				account.setAccountBalance(resultSet.getDouble("ACCOUNT_BALANCE"));
				account.setStatus1(resultSet.getString("ACCOUNT_STATUS"));
//				System.out.println(account);
			}
			double balance = account.getAccountBalance();
			String accountStatus = account.getStatus1();
			System.out.println("Your pre-transaction balance: $" + balance);
			if(withdrawl <= 0 || withdrawl >= balance) {
				System.out.println("Sorry! Invalid withdrawl amount.");
			} else if (accountStatus == "PENDING") {
				System.out.println("Sorry! Your account hasn't been approved yet.");
			} else if (accountStatus == "REJECTED") {
				System.out.println("Sorry! This is a rejected account.");
			} else {
				balance-= withdrawl;
				System.out.println("Withdrawl Successful!");
			}
			String sql2  = "UPDATE BANK_ACCOUNTS SET ACCOUNT_BALANCE = ? WHERE USERNAME = ? AND ACCOUNT_NUMBER = ?";
			Connection connection2 = ConnectionUtil.getConnection();
			PreparedStatement pStatement2 = connection2.prepareStatement(sql2);
			pStatement2.setDouble(1, balance);
			pStatement2.setString(2, username);
			pStatement2.setInt(3, accountNumber);
			ResultSet resultSet2 = pStatement2.executeQuery();
			
			System.out.println("Current Balance is: $" + balance);
				
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public User createUser(String username, String password) {
		try {
			String sql = "INSERT INTO BANK_USERS (USERNAME, PASSWORD) VALUES (?, ?)";
			Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pStatement = connection.prepareStatement(sql);
			pStatement.setString(1, username);
			pStatement.setString(2, password);
			ResultSet resultSet = pStatement.executeQuery();
			User user1 = new User(); 
			while(resultSet.next()) {
				
				user1.setUserNumber(resultSet.getInt("USER_NUMBER"));
				user1.setUsername(resultSet.getString("USERNAME"));
				user1.setPassword(resultSet.getString("PASSWORD"));
				user1.setEmployeeID(resultSet.getInt("EMPLOYEE_ID"));
//				System.out.println(user);
			}
			int uNumber = user1.getUserNumber();
			String uName = user1.getUsername();
			String pWord = user1.getPassword();
			
			System.out.println("Your new account details: User_Number= " + uNumber + ", Username= " + uName + " Password= " + pWord  );
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return null;
	}

}
