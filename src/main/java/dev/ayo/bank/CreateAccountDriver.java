package dev.ayo.bank;

import java.util.Scanner;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

import dev.ayo.bank.Account;
import dev.ayo.bank.User;
import dev.ayo.daos.AccountDaoImpl;
import dev.ayo.util.ConnectionUtil;

public class CreateAccountDriver {

	public static void main(String[] args) throws Exception {
		
		Account account = new Account();
		User user = new User();
		Scanner scanner = new Scanner(System.in);
		AccountDaoImpl acDao = new AccountDaoImpl();
		
		System.out.println("Please enter your username and initial balance to create a new account");
		String uName1 = scanner.next();
		double initBal = scanner.nextDouble();
		acDao.createAccount(uName1,initBal);	
		
//		acDao.deposit("UzamkiNaruto", 10, 10);
//		acDao.withdrawl("UzamkiNaruto", 10, 10);
//		acDao.rejectAccount("UzamkiNaruto", 10);
//		acDao.approveAccount("UzamkiNaruto", 14);
//		acDao.createUser("Ekko", "Chronobreak");
//		acDao.getAccountInformation("UzamkiNaruto");
		
	}

}
