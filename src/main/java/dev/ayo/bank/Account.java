package dev.ayo.bank;


public class Account {

	private int accountNumber;
	private String username;
	private double accountBalance;
	private AccountStatus status;
	private String status1;
	private double initialBalance;
	
	
	public Account() {
		super();
	}
	
	public int getAccountNumber() {
		return accountNumber;
	}
	
	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public double getAccountBalance() {
		return accountBalance;
	}
	
	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}
	
	public AccountStatus getStatus() {
		return status;
	}
	
	public void setStatus(AccountStatus status) {
		this.status = status;
	}
	
	public String getStatus1() {
		return status1;
	}

	public void setStatus1(String status1) {
		this.status1 = status1;
	}

	public double getInitialBalance() {
		return initialBalance;
	}
	
	public void setInitialBalance(double initialBalance) {
		this.initialBalance = initialBalance;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(accountBalance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + accountNumber;
		temp = Double.doubleToLongBits(initialBalance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((status1 == null) ? 0 : status1.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (Double.doubleToLongBits(accountBalance) != Double.doubleToLongBits(other.accountBalance))
			return false;
		if (accountNumber != other.accountNumber)
			return false;
		if (Double.doubleToLongBits(initialBalance) != Double.doubleToLongBits(other.initialBalance))
			return false;
		if (status != other.status)
			return false;
		if (status1 == null) {
			if (other.status1 != null)
				return false;
		} else if (!status1.equals(other.status1))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Account [accountNumber= " + accountNumber + ", username= " + username + ", accountBalance= $"
				+ accountBalance + ", status= " + status + ", status1= " + status1 + "]";
	}
	
	
	public String toString2() {
		return "Account [accountNumber=" + accountNumber + ", username=" + username + ", accountBalance= $"
				+ accountBalance + ", status=" + status + ", status1= " + status1 + ", initialBalance=" + initialBalance + "]";
	}
	

	
	
	
	
//	public void deposit(double amount) throws Exception {
//		
//		if(amount<0) {
//			throw new Exception("Amount cannot be less than zero.");
//		}
//		balance +=amount;		
//	}
//	
//	public void withdraw(double amount) throws Exception {
//		
//		if(amount<0) {
//			throw new Exception("Amount cannot be less than zero.");
//		}
//		
//		if(amount>balance) {
//			throw new Exception("Balance is too low for this transaction.");
//		}
//		balance -=amount;				
//	}

}
