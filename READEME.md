<h1>Welcome to Ayo Bank</h1>
<h3>*********************<h3>
<h2>This CLI app is built using</h2>
<ul>
<li>Java maven v1.8</li>
<li>AWS: Cloud Computing platform, via Relational Database Services </li>
<li>DBeaver: a SQL client Integrated Development Environment toolkit</li>
<li>JDBC (Java Database Connectivity): the Java API which connects and integrates JAVA applications with databases </li>
</ul>
<h3>*********************<h3>
<h2>Maven dependencies used</h2>
<ul>
<li>ojdbc and</li>
</ul>
<h3>*********************<h3>
<h2>Functionalities</h2>

<ul>
<li>A customer can apply for a new bank account with a starting balance (default - $100.00).</li>
<li>A customer can view the balances of all their accounts.</li>
<li>A customer can make a withdrawal or deposit to a specific account.</li>
<li>As the system, invalid transactions are rejected. Examples:
<ul>
<li>Account with a PENDING or REJECTED status</li>
<li>A deposit or withdrawal of negative money.</li>
<li>A withdrawal that would result in a negative balance.</li>
</ul>
</li>
<li>An employee can accept or reject a pending account.</li>
</ul>

